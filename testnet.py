import network, time
import machine
from umqttsimple import MQTTClient
import ubinascii
import netconstants

from setnet import do_connect, connect_and_subscribe, restart_and_reconnect

# essid='Livebox-0A00'
# wlanpass='mjxriXajNsHqLQUxKT'
# mqtt_server='marvin'
client_id=ubinascii.hexlify(machine.unique_id())
topic_sub='testrep'
message_interval=1
last_message=0
counter=0

do_connect()

try:
  client = connect_and_subscribe(client_id, mqtt_server, topic_sub)
except OSError as e:
  restart_and_reconnect()

while True:
  try:
    client.check_msg()
    if (time.time() - last_message) > message_interval:
      msg = b'Hello #%d' % counter
      client.publish('test', msg)
      last_message = time.time()
      counter += 1
  except OSError as e:
    restart_and_reconnect()
