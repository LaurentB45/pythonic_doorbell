from machine import Pin
import uasyncio as asyncio

runapp = Pin(12, Pin.IN, Pin.PULL_UP)
# global led = Pin(0, Pin.OUT)

def empty ():
    print ('Empty was here') 

async def run_every(  fn ,  minute = 1, sec=None):
    print("entered run_every parameters function {fun} sec {sec} min {minute} ".format( fun = fn , sec = sec, minute = minute))
    led  = Pin(0, Pin.OUT)
    wait_sec = (sec if sec else minute * 60)
    while True:
        led.value(1)
        try:
            print("loop")
            fn()
            # empty()
        except Exception:
            print( "run_every catch exception for %s" % fn)
            raise
        await asyncio.sleep ( 0.1 )
        led.value(0)
        await asyncio.sleep( wait_sec )

async def run_app_exit():
    global runapp
    while( runapp.value()==1 ):
        await asyncio.sleep(10)
    return
