import time
import network
import machine
import ubinascii
from umqttsimple import MQTTClient
import netconstants as nc # The "constants" to manage network connections

def do_connect():
    sta_if = network.WLAN(network.STA_IF)
    ap_if = network.WLAN(network.AP_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(nc.essid, nc.wlanpass)
        ctime=time.time()
        while not sta_if.isconnected():
            if time.time()-ctime > 20:
                print('Network timeout')
                break
            time.sleep(0.5)
    print('network config:', sta_if.ifconfig())
    ap_if.active(False)


def sub_cb(topic, msg):
    print((topic, msg))
    if topic == b'notification' and msg == b'received':
        print('ESP received hello message')


def connect_and_subscribe(client_id, mqtt_server, topic_sub):
    client = MQTTClient(client_id, mqtt_server)
    client.set_callback(sub_cb)
    client.connect()
    client.subscribe(topic_sub)
    print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
    return client


def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()
