Project: pythonic_doorbell

This projects aims to create a modern doorbell using Feather boards, micropython and MQTT.

The expected functionalities:
    - work as a doorbell
    - can have several terminals, each can be an emitter or receiver of signals
    - can manage acknowledgement for the family members to notify others of an "alert" and confirm the heard said "alert"

