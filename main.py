# LBA 202106
# Main script for ESP8266 "Feather" under micro python
# This version reads a BMP180 every 30 seconds and
# publishes via MQTT under the "bmp/"
# topics (press and temp)


import uasyncio as asyncio
import ubinascii
import setnet as net
from machine import Pin, I2C, unique_id
from bmp180 import BMP180


runapp = Pin(12, Pin.IN, Pin.PULL_UP)
led = Pin(0, Pin.OUT)


def empty():
    pass


async def run_every(fn,  minute=1, sec=None):
    print("entered run_every parameters function "
          + "{fun} sec {sec} min {minute} ".
          format(fun=fn, sec=sec, minute=minute))
    # led = Pin(0, Pin.OUT)
    wait_sec = (sec if sec else minute * 60)
    while True:
        led.value(1)
        try:
            # print("loop")
            fn()
            # empty()
        except Exception:
            print("run_every catch exception for %s" % fn)
            raise
        await asyncio.sleep(0.1)
        led.value(0)
        await asyncio.sleep(wait_sec)


async def run_app_exit():
    global runapp
    while runapp.value() == 1:
        await asyncio.sleep(10)
    return


def initbmp():
    # Bus I2C
    #   Ne pas utiliser la broche standard SCL (broche 5) car perturbe la
    #   sequence de boot lorsque l'on utilise un bloc d'alim USB
    #

    # 0 précision la plus basse, mesure rapide
    # 3 précision la plus élevée; mesure plus lente
    bmp180.oversample_sett = 2

    # Pression au niveau de la mer (en millibar * 100)
    bmp180.baseline = 101325

    return bmp180


def readbmp():

    # Température sur le BMP
    temp = bmp180.temperature
    print("Temperature: %.2f deg.Celsius" % temp)

    msg = b'Temp %d' % temp
    client.publish('bmp/temp', msg)

    p = bmp180.pressure
    print("pressure: %.2f hPa" % (p/100))

    # Altitude calculée a partir de la difference de pression
    # entre le niveau de la mer et "ici"
    altitude = bmp180.altitude
    print("altitude: %.2f m" % altitude)

    msg = "Pres {:.2f}".format(p/100)
    client.publish('bmp/press', msg)


##############################################################################
# Main
##############################################################################
if runapp.value() == 0:
    print("arret application, Runapp=0")
else:
    print("execution application, Runapp = 1")

print("Init mqtt")
client_id = ubinascii.hexlify(unique_id())
client = net.connect_and_subscribe(client_id,
                                   nc.mqtt_server,
                                   'test')

print("Init bmp")
i2c = I2C(sda=Pin(4), scl=Pin(2), freq=20000)
bmp180 = BMP180(i2c)
initbmp()
readbmp()

print("creating loop")

loop = asyncio.get_event_loop()

print("creating task")

loop.create_task(run_every(empty, sec=5))
loop.create_task(run_every(readbmp, sec=30))

print("created task")

try:
    loop.run_until_complete(run_app_exit())
except Exception as e:
    print(e)
#    led_error(step=6)
