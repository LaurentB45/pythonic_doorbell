# Utilisation du breakout BMP180 (ADA1603) avec Feather ESP8266 Python
#
# Shop: http://shop.mchobby.be/product.php?id_product=397
# Wiki: https://wiki.mchobby.be/index.php?title=MicroPython-Accueil#ESP8266_en_MicroPython

from bmp180 import BMP180
from machine import I2C, Pin

# Bus I2C
#   Ne pas utiliser la broche standard SCL (broche 5) car perturbe la
#   sequence de boot lorsque l'on utilise un bloc d'alim USB
# 
i2c = I2C( sda=Pin(4), scl=Pin(2), freq=20000 )

bmp180 = BMP180( i2c )

# 0 précision la plus basse, mesure rapide
# 3 précision la plus élevée; mesure plus lente
bmp180.oversample_sett = 2 

# Pression au niveau de la mer (en millibar * 100)
#bmp180.baseline = 101325
bmp180.baseline = 101600

# Température sur le BMP
temp = bmp180.temperature
print( "Temperature: %.2f deg.Celcius" % temp )

p = bmp180.pressure
print( "pressure: %.2f mbar" % (p/100) )
print( "pressure: %.2f hPa" % (p/100) )

# Altitude calculée a partir de la difference de pression 
# entre le niveau de la mer et "ici"
altitude = bmp180.altitude
print( "altitude: %.2f m" % altitude )
